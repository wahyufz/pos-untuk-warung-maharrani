<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title') | Maharrani</title>

    <script type="text/javascript" src="{{ URL::asset('node_modules/jquery/dist/jquery.js')}}"></script>

    <!-- angularjs script  -->
    <script type="text/javascript" src="{{ URL::asset('node_modules/angular/angular.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ URL::asset('tambahan/angular_bracket.js') }}" type="text/javascript"></script>

    <link href="{{ URL::asset('singlecolor/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('singlecolor/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('singlecolor/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('singlecolor/css/animate.min.css')}}" rel="stylesheet">
  	<link href="{{ URL::asset('singlecolor/css/main.css')}}" rel="stylesheet">
  	<link href="{{ URL::asset('singlecolor/css/responsive.css')}}" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ URL::asset('singlecolor/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('singlecolor/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('singlecolor/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('singlecolor/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('singlecolor/images/ico/apple-touch-icon-57-precomposed.png')}}">


	  <link rel="stylesheet" href="{{ URL::asset('node_modules/jqueryui/jquery-ui.css')}}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css"> --}}
    <link rel="stylesheet" href="{{ URL::asset('node_modules/jquery-confirm/css/jquery-confirm.css')}}">
</head><!--/head-->


<body ng-app="App">
	<header id="header">
        <div class="navbar navbar-inverse" role="banner">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="/">
                        <h1>
                          Maharrani
                        </h1>
                    </a>

                </div>
                @include('layouts.menuBar')
            </div>
        </div>
    </header>
    <!--/#header-->

    <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container-fluid">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">@yield('title')</h1>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>

   @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
   @endif

    <!--/#action-->
    <footer id="footer">
        <div class="container-fluid">
            <div class="row">
                {{-- <div class="col-sm-12"> --}}
                  @yield('content')
                {{-- </div> --}}
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Ahsan Comp 2018. All Rights Reserved.</p>
                        <p>Crafted by <a target="_blank" href="http://designscrazed.org/">Allie</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->
    <script type="text/javascript" src="{{ URL::asset('singlecolor/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('singlecolor/js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('singlecolor/js/main.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('node_modules/jqueryui/jquery-ui.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ URL::asset('node_modules/jquery-confirm/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('tambahan/angular_datepicker.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
          $( ".datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
        });
    </script>
</body>
</html>
