<div class="collapse navbar-collapse">
  <ul class="nav navbar-nav navbar-right">
    @if (Auth::check())
      <li><a href="{{url('/')}}">Home</a></li>
      <li><a href="{{url('/user')}}">User</a></li>
      <li class='dropdown'><a href="#">Menu <i class="fa fa-angle-down"></i></a>
        <ul role="menu" class="sub-menu">
          <li><a href="{{url('/barang')}}">Semua Menu</a>
          <li><a href="{{ url('/barang/makanan_aktif') }}">Makanan</a></li>
          <li><a href="{{ url('/barang/minuman_aktif') }}">Minuman</a></li>
          <li><a href="{{ url('/barang/setok_aktif') }}">lain - lain</a></li>
        </ul>
      </li>
      <li class='dropdown'><a href="#">Nota <i class="fa fa-angle-down"></i></a>
        <ul role="menu" class="sub-menu">
          <li><a href="{{ url('/nota') }}">Nota Hari Ini</a></li>
          <li><a href="{{ url('/nota/lunas') }}">Nota Lunas</a></li>
        </ul>
      </li>
      <li class='dropdown'><a href="#">{{Auth::user()->nama}} <i class="fa fa-angle-down"></i></a>
        <ul role="menu" class="sub-menu">
          <li><a href="{{url('/setting')}}">Setting</a></li>
          <li><a href="{{url('/logout')}}">Logout</a></li>
        </ul>
      </li>
      <li>

      </li>
    @else
      <li><a href="{{url('/login')}}">Login </a>
      </li>
    @endif
  </ul>
</div>
