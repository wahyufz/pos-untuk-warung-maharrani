@extends('layouts.header_footer')
@section('title')
    Daftar User {{ ucwords($status) }}
@endsection

@section('content')
<div class="col-md-12 col-sm-8 ">
    <link href="{{ URL::asset('tambahan/IndexShow.css')}}" rel="stylesheet">
    <a href="{{ url('/user/aktif') }}" class="btn btn-success">User Aktif</a>
    <a href="{{ url('/user/nonaktif') }}" class="btn btn-danger">User Nonaktif</a>
    <a href="{{ url('/user/create') }}" class="btn btn-primary">Tambah User</a><br /><br />
    @if ($jumlahUser>0)
      <div class="table-responsive">
        <table class="table">
        <thead>
          <th class="table-nama">Nama</th>
          <th class="table-posisi">Posisi</th>
          <th class="table-action"></th>
        </thead>
        <tbody>
          @foreach ($daftarUser as $key => $value)
            <tr>
              <td style="text-align:left;">
                {{ $value->nama }}
              </td>
              <td>
                {{ $value->posisi }}
              </td>
              <td>
                {!! Form::open(['method' => 'DELETE', 'action'=>['userController@destroy',$value->id]]) !!}
                <a href="{{ url('/user/'.$value->id.'/edit') }}" class="btn btn-warning ">Edit</a>
                {!! Form::hidden('id', $value->id) !!}
                @if ($value->status=='aktif')
                  {!! Form::button('Nonaktifkan', array('type' => 'submit', 'class' => 'btn btn-danger ')) !!}
                @elseif ($value->status=='nonaktif')
                  {!! Form::button('Aktifkan', array('type' => 'submit', 'class' => 'btn btn-success ')) !!}
                @endif
                {!! Form::close() !!}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      </div>
      <br />
      Jumlah User @if (isset($status))
        {{ ucwords($status) }}
      @endif {{ $jumlahUser }} orang <br>
      {{ $daftarUser->links() }}
    @elseif ($jumlahUser==0)
      User  {{ ucwords($status) }} Kosong
    @endif
</div>
@endsection
