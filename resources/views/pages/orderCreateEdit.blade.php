@extends('layouts.header_footer')
@section('title')
  {{ ucwords($judul) }} Order Nomor Meja {{ $nota->nomor_meja }}
@endsection

@section('content')
  <style media="screen">
  .col{
    margin-bottom: 15px;
  }
  </style>
  <link rel="stylesheet" href="{{URL::asset('node_modules/selectize/dist/css/selectize.css')}}" />
  <script src="{{URL::asset('node_modules/selectize/dist/js/standalone/selectize.min.js')}}"></script>
  <link rel="stylesheet" href="{{URL::asset('tambahan/handleCounter.css')}}" />
  <script src="{{URL::asset('tambahan/handleCounter.js')}}"></script>
  {{-- <div class="col-md-12 col-sm-12 "> --}}
  <link href="{{ URL::asset('tambahan/IndexShow.css')}}" rel="stylesheet">
  <form action="/order{{ $judul=="Edit" ? '/'.$order->id : '' }}" method="post">
    {{ csrf_field() }}
    <div class="col-md-12 text-center col">
      <button type="submit" {{ $judul=='Edit' ? "name=_method value=PATCH" : '' }} class="btn btn-success btn-lg">Simpan</button>
    </div>

    @php
    if ($judul=='Edit') {
      $loop = 1;
    }else {
      $loop = 20;
    }
    @endphp
    @for ($i = 1; $i <= $loop; $i++)
      <input type="hidden" name="{{$i}}[idNota]" value="{{ $nota->id }}">
      <div class="col-sm-12 visible-xs-block visible-sm-block text-center col" style="border-top: 1px solid black">
        <h3>Pesanan {{ $i }}</h3>
      </div>
      <div class="col-sm-12 col-md-4 col">
        <select class="selectize" name="{{$i}}[idBarang]">
          @if($judul == 'Edit')
            <option value="{{ $order->idBarang }}" selected>
            </option>
          @endif
        </select>
        <input type="number" min="0" step="500" class="form-control" style="margin-top:15px" name="{{$i}}[diskon]" placeholder="Potongan Harga" value="{{$judul=='Edit' ? $order->diskon : ''}}">
      </div>
      <div class="col-sm-12 col-md-2 text-center col" >
        <div class="handle-counter" id="handleCounter{{ $i }}">
          <p class="counter-minus btn btn-primary btn-lg">-</p>
          <input type="text" name="{{$i}}[kuantitas]" value="{{$judul=='Edit' ? $order->kuantitas : '0'}}" >
          <p class="counter-plus btn btn-primary btn-lg">+</p>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col">
        <textarea name="{{$i}}[catatan]" class="form-control" placeholder="Catatan Tambahan" rows="6" >{{$judul=='Edit' ? str_replace("<br />", "\n", $order['catatan']) : ''}}</textarea>
      </div>

    @endfor

  </form>
  <script type="text/javascript">
  $(document).ready(function() {

    @for ($i = 1; $i <= 20; $i++)
    $('#handleCounter{{ $i }}').handleCounter({minimum:{{$judul == 'Edit' ? '1' : '0'}} });
    @endfor

    var data = [
      {
        'id': 0,
        'text': 'Daftar Menu'
      },
      @foreach ($daftarBarang as $barang)
      {
        'id': "{{ $barang->id }}",
        'text': '{{ $barang->nama }} - Rp.{{ number_format($barang->harga_jual) }}',
      },
      @endforeach
    ];

    $('.selectize').selectize({
      maxOptions: 20,
      valueField: 'id',
      labelField: 'text',
      searchField: 'text',
      sortField: 'text',
      options: data,
      create: false
    });
  });

</script>
@endsection
