@extends('layouts.header_footer')
@section('title')
    Daftar {{ ucwords($judul) }}
@endsection

@section('content')
<div class="col-md-12 col-sm-12 ">
    <link href="{{ URL::asset('tambahan/IndexShow.css')}}" rel="stylesheet">
    @if ($judul!='seluruhnya')
      <a href="{{ url('/barang/'.$jenis.'_aktif') }}" class="btn btn-success">{{ ucwords($jenis) }} Aktif</a>
      <a href="{{ url('/barang/'.$jenis.'_nonaktif') }}" class="btn btn-danger">{{ ucwords($jenis) }} Nonaktif</a>
    @endif
    <a href="{{ url('/barang/create') }}" class="btn btn-primary">Tambah Barang</a><br /><br />
    <form action="/search/barang" method="get">
      <input type="radio" name="jns" value="makanan">Makanan
      <input type="radio" name="jns" value="minuman">Minuman
      <input type="radio" name="jns" value="snack">Snack
      <br>
      <input type="text" name="k" />
      <input type="submit" class="btn btn-warning" value="cari"/>
    </form>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <th class="table-nama">Nama</th>
          <th class="table-jenis">Jenis</th>
          <th class="table-harga">Harga Jual</th>
          <th class="table-action"></th>
        </thead>
        <tbody>
          @foreach ($daftarBarang as $key => $value)
            <tr>
              <td style="text-align:left;">
                {{ $value->nama }}
              </td>
              <td>
                {{ $value->jenis }}
              </td>
              <td class="uang">
                {{ number_format($value->harga_jual) }}
              </td>
              <td>
                {!! Form::open(['method' => 'DELETE', 'action'=>['barangController@destroy',$value->id]]) !!}
                <a href="{{ url('/barang/'.$value->id.'/edit') }}" class="btn btn-warning ">Edit</a>
                {!! Form::hidden('id', $value->id) !!}
                @if ($value->status=='aktif')
                  {!! Form::button('Non-aktifkan', array('type' => 'submit', 'class' => 'btn btn-danger ')) !!}
                @elseif ($value->status=='nonaktif')
                  {!! Form::button('Aktifkan', array('type' => 'submit', 'class' => 'btn btn-success ')) !!}
                @endif
                {!! Form::close() !!}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <br />
     JUMLAH MENU {{ $daftarBarang->total() }} <br>
     {{ $daftarBarang->links() }}
</div>
@endsection
