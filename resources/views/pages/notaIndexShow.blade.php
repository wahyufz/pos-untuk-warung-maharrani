@extends('layouts.header_footer')
@section('title')
    Daftar Nota {{ ucwords($judul) }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 ">
      <style media="screen">
        .a{
          vertical-align: middle;
        }
      </style>
        <link href="{{ URL::asset('tambahan/IndexShow.css')}}" rel="stylesheet">
        <form action="/nota" method="post">
          <label for="nomor_meja">Nomor Meja </label>
          {{ csrf_field() }}
          <input type="number" name="nomor_meja" max="999" min="1" style="height:34px;width:80px;padding-top:1px;">
          <input type="submit" value="Tambah" class="btn btn-primary">
          @if (count($daftarNota['proses'])>0)
            <h2 class="text-center">Nota Proses</h2>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <th class="col-md-2"></th>
                  <th>Nomor Meja</th>
                  <th>Pembuat</th>
                  <th>Waktu</th>
                  <th>Status Pengiriman</th>
                </thead>
                <tbody>
                  @foreach ($daftarNota['proses'] as $key => $value)
                    <tr>
                      <td>
                        <a href="{{ url('/nota/'.$value->id.'/edit') }}" class="btn btn-warning ">Edit</a>
                      </td>
                      <td>
                        {{ $value->nomor_meja }}
                      </td>
                      <td>
                        {{ $value->inputter->nama }}
                      </td>
                      <td>
                        {{ $value->created_at->toTimeString() }}
                      </td>
                      <td>
                        Status Pengiriman
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </form>
              </table>
            </div>
            JUMLAH NOTA PROSES {{ $daftarNota['proses']->count() }} <br>
          @else
            <div class="text-center">
              <h2>Nota Proses Kosong</h2>
            </div>
          @endif
          @if (count($daftarNota['lunas'])>0)
            <h2 class="text-center">Nota Lunas</h2>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <th class="col-md-2"></th>
                  <th>Nomor Nota</th>
                  <th>Kasir</th>
                  <th>Waktu Pembayaran</th>
                  <th>Diskon</th>
                  <th>Total</th>
                  <th>PPn 10%</th>
                  <th>Total Akhir</th>
                </thead>
                <tbody >
                  @foreach ($daftarNota['lunas'] as $key => $value)
                    <tr>
                      <td>
                        @if ($value->status=='batal')
                          <a href="{{ url('/nota/'.$value->id.'/edit') }}" class="btn btn-danger ">Nota Batal</a>
                        @else
                          <a href="/print/nota/{{ $value->id }}" target="_blank" class="btn btn-success">Print</a>
                          <a href="{{ url('/nota/'.$value->id.'/edit') }}" class="btn btn-warning ">Detail</a>
                        @endif
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ $value->nomor_meja }}
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ $value->inputter->nama }}
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ $value->updated_at->toTimeString() }}
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ number_format($value->diskon) }}
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ number_format($value->total) }}
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ number_format($value->ppn) }}
                      </td>
                      <td style=" vertical-align:middle; ">
                        {{ number_format($value->total + $value->ppn) }}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="3"></td>
                    <td>Total</td>
                    <td>{{ number_format($daftarNota['total']['diskon']) }}</td>
                    <td>{{ number_format($daftarNota['total']['total']) }}</td>
                    <td>{{ number_format($daftarNota['total']['ppn']) }}</td>
                    <td>{{ number_format($daftarNota['total']['ppn'] + $daftarNota['total']['total']) }}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
            JUMLAH NOTA LUNAS {{ $daftarNota['lunas']->count() }} <br>
          @else
            <div class="text-center">
              <h2>Nota Lunas Kosong</h2>
            </div>
          @endif
    </div>
@endsection
