@extends('layouts.header_footer')
@section('title')
  {{ ucwords($judul) }} {{$judul == "Edit" ? "Nomor Meja" : "Nomor Nota "}} {{ $nota->nomor_meja }}
@endsection

@section('content')
  <link href="{{ URL::asset('tambahan/IndexShow.css')}}" rel="stylesheet">
  <div class="col-md-12 col">
    <form action="" method="post">
      <a href="/nota" class="btn btn-primary">Kembali</a>
      @if ($nota->status == 'proses')
        <a href="/order/{{ $nota->id }}/create" class="btn btn-success">Order Menu</a>
        <button type="submit" name="_method" class="btn btn-danger" value="DELETE" formaction="/nota/{{ $nota->id }}">Batalkan Nota</button>
      @elseif ($nota->status == 'lunas')
        <a href="/print/nota/{{ $nota->id }}" target="_blank" class="btn btn-success">Print</a>
      @endif
      {{ csrf_field() }}
      @if ($nota->order->count() > 0)
        <div class="table-responsive">
          <table class="table" >
            <thead>
              @if ($nota->status == 'proses')
                <th style="width:30px">
                  <input type="checkbox" onclick="centangSemua(this)" />
                </th>
              @endif
              <th>Catatan</th>
              <th>Menu</th>
              <th class="uang">Harga</th>
              <th class="uang">Kuantitas</th>
              <th class="uang">Diskon</th>
              <th class="uang">Total</th>
              <th class="uang"></th>
            </thead>
            <tbody>
              @foreach ($nota->order as $element)
                <tr>
                  @if ($nota->status == 'proses')
                    <td>
                      <input type="checkbox" class="cb" name="pesanan[{{ $element->id }}]" />
                    </td>
                  @endif
                  <td>{!! $element->catatan !!}</td>
                  <td>{{ $element->barang->nama }}</td>
                  <td>{{ number_format($element->harga_jual) }}</td>
                  <td>{{ number_format($element->kuantitas) }}</td>
                  <td>{{ number_format($element->diskon) }}</td>
                  <td>
                    {{ number_format(($element->barang->harga_jual * $element->kuantitas) - $element->diskon) }}
                  </td>
                  <td>
                    @if ($nota->status == 'proses' && $element->status != 'cancel')
                      <a href="/order/{{ $element->id }}/edit" class="btn btn-warning">Edit</a><br>
                      {{-- <a href="/order/{{ $element->id }}/kirim" class="btn btn-success">Kirim</a><br> --}}
                      <button type="submit" name="_method" class="btn btn-danger" value="DELETE" formaction="/order/{{ $element->id }}">Batalkan</button>
                    @elseif ($element->status == 'cancel')
                      Pesanan Batal
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>

            <tfoot
            ng-init="
            total='{{ $nota->status == 'proses' ? $nota->akhir['total'] : $nota->total }}';
            ppn10='{{ $nota->status == 'proses' ? round($nota->akhir['total']*0.1 ) : $nota->ppn}}';
            ppn = ppn10">

              <tr>
                <td colspan="{{$nota->status == 'proses' ? '5' : '4' }}" style="text-align:left;padding-left:23px;">
                  <div class="">
                    @if ($nota->status == 'proses')
                      <button type="submit"class="btn btn-danger" name="_method" value="DELETE" formaction="/order/hapusbanyak">Batalkan</button>
                    @endif
                  </div>
                </td>
                <td>Total</td>
                <td>{+ total | number:0 +}</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="{{$nota->status == 'proses' ? '5' : '4' }}" ></td>
                <td>PPn 10%</td>
                <td>{+ ppn | number:0 +}</td>
                <td>
                  @if ($nota->status=='proses')
                    <input type="checkbox" ng-model="add" ng-change="ppn = add * ppn10" id="ppn"/>
                    <input type="hidden" name="ppn" value="{+ ppn +}"/>
                  @endif
                </td>
              </tr>
              <tr>
                <td colspan="{{$nota->status == 'proses' ? '5' : '4' }}" ></td>
                <td>Total Akhir</td>
                <td>{+ totalAkhir = (total-0)+(ppn-0) | number:0 +}</td>
                <td></td>
              </tr>
              @if ($nota->status=='proses')
                <tr style="height:40px">
                  <td colspan="{{$nota->status == 'proses' ? '5' : '4' }}" ></td>
                  <td> Bayar </td>
                  <td>
                    @if ($nota->status == 'proses')
                      <input type="number" class="noEnter" style="width:120px;height:50px;" name="bayar" ng-model="bayar" class="form-control" placeholder="Pembayaran" autocomplete="off" style="height:40px;">
                    @else
                      {{ number_format($nota->bayar) }}
                    @endif
                  </td>
                  <td>
                    @if ($nota->status == 'proses')
                      <button type="submit" name="_method" value="PATCH" formaction="/nota/{{ $nota->id }}" class="btn btn-success" >Bayar</button>
                    @endif
                  </td>
                </tr>
                <tr>
                  <td colspan="{{$nota->status == 'proses' ? '5' : '4' }}" ></td>
                  <td>Kembali</td>
                  <td>
                    @if ($nota->status == 'proses')
                      {+ kembalian = bayar - totalAkhir | number:0 +}
                      <input type="hidden" name="kembalian" value="{+ kembalian +}" />
                    @else
                      {{ $nota->kembalian }}
                    @endif
                  </td>
                  <td></td>
                </tr>
              @endif
            </tfoot>
          </table>
        </div>
      @else
        <h4>Nota Masih Kosong</h4>
      @endif
    </form>
  </div>
  <br>
  <script type="text/javascript">
  $(function(){
    $("#ppn").val('true');
  });
  var input = Array.from(document.getElementsByClassName('noEnter'));
  for (var i = 0; i < input.length; i++) {
    input[i].addEventListener('keydown',function (event) {
      if (event.keyCode == 13) {
        event.preventDefault();
      }
    });
  }
  function centangSemua(source) {
    checkboxes = Array.from(document.getElementsByClassName('cb'));
    for (var i = 0; i < checkboxes.length; i++) {
      checkboxes[i].checked = source.checked;
    };
  }
  </script>
@endsection
