@extends('layouts.header_footer')
@section('title')
  {{ ucwords($route) }}
@endsection

@section('content')
    <div class="col-md-6 col-sm-8 ">

        @include('pages.form_error_list')
        @if ($route=='tambah')
            {!! Form::open(['method' => 'POST', 'action'=>['barangController@store']]) !!}
        @elseif ($route=='edit')
            {!! Form::model($barang,['method' => 'PATCH', 'action'=>['barangController@update',$barang->id]]) !!}
        @endif

            {!! Form::hidden('status','aktif') !!}
            <!-- input nama -->
            <div class="form-group">
                {!! Form::label('nama','Nama ',['class' => 'control-label']) !!}
                {!! Form::text('nama',null,['class' => 'form-control ', 'placeholder'=> 'Nama']) !!}
            </div>
            <!-- /#input nama -->

            <!-- select jenis -->
            <div class="form-group">
                {!! Form::label('jenis','jenis',['class' => 'control-label']) !!}<br>
                {!! Form::radio('jenis','makanan',true) !!} Makanan<br>
                {!! Form::radio('jenis','minuman') !!} Minuman<br>
                {!! Form::radio('jenis','setok') !!} Lain - Lain<br>
                {!! Form::radio('jenis','mentah') !!} Bahan Mentah<br>
            </div>
            <!-- /#select jenis -->

            <!-- input harga -->
            <div class="form-group">
                {!! Form::label('harga_jual','Harga ',['class' => 'control-label ']) !!}
                {!! Form::number('harga_jual',null,['class' => 'form-control', 'placeholder'=> 'Harga']) !!}
            </div>
            <!-- /#input harga -->

            <!-- submit-->
            <div class="form-group">
                <br />
                {!! Form::submit($route,['class' => 'btn btn-submit']) !!}
            </div>
            <!-- /#submit-->

        {!! Form::close() !!}

    </div>
@endsection
