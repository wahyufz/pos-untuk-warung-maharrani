@extends('layouts.header_footer')
@section('title')
    {{ ucwords($route) }} User
@endsection

@section('content')
    <div class="col-md-6 col-sm-8 ">

        @include('pages.form_error_list')
        @if ($route=='tambah')
            {!! Form::open(['method' => 'POST', 'action'=>['userController@store']]) !!}
        @elseif ($route=='edit')
            {!! Form::model($user,['method' => 'PATCH', 'action'=>['userController@update',$user->id]]) !!}
            {!! Form::hidden('id',$user->id) !!}
        @endif

            <!-- input nama -->
            <div class="form-group">
                {!! Form::label('nama','Nama ',['class' => 'control-label']) !!}
                {!! Form::text('nama',null,['class' => 'form-control ', 'placeholder'=> 'Nama']) !!}
            </div>
            <!-- /#input nama -->

            <!-- input password -->
            <div class="form-group">
                {!! Form::label('password','Password',['class' => 'control-label']) !!}
                @if ($route == 'edit')
                  (Kosongkan jika tidak merubah Password)
                @endif
                {!! Form::password('password',['class' => 'form-control', 'placeholder'=> 'Password']) !!}
            </div>
            <!-- /#input Password -->

            <!-- input ulangi password -->
            <div class="form-group">
                {!! Form::label('password_confirm','Ulangi Password',['class' => 'control-label']) !!}
                @if ($route == 'edit')
                  (Kosongkan jika tidak merubah Password)
                @endif
                {!! Form::password('password_confirm',['class' => 'form-control', 'placeholder'=> 'Ulangi Password']) !!}
            </div>
            <!-- /#input ulangi Password -->

            <!-- select posisi -->
            <div class="form-group">
                {!! Form::label('posisi','Posisi',['class' => 'control-label']) !!}<br>
                {!! Form::radio('posisi','admin') !!} Admin<br>
                {!! Form::radio('posisi','spv') !!} Spv<br>
                {!! Form::radio('posisi','kasir',true) !!} kasir<br>
            </div>
            <!-- /#select posisi -->

            <!-- submit-->
            <div class="form-group">
                <br />
                {!! Form::submit($route,['class' => 'btn btn-submit']) !!}
            </div>
            <!-- /#submit-->

        {!! Form::close() !!}
    </div>
@endsection
