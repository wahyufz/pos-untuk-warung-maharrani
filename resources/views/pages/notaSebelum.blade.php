@extends('layouts.header_footer')
@section('title')
  Nota {{ucwords($tgl)}}
@endsection

@section('content')
  <div class="col-md-12 col-sm-12 ">
    <form action="/nota/lunas" method="get">
      <input type="date" name="tgl" style="width:150px;height:30px" data-date-format="DD MMMM YYYY" />
      <input type="submit"  value="Lihat Catatan" class="btn btn-primary" />
    </form>
    <link href="{{ URL::asset('tambahan/IndexShow.css')}}" rel="stylesheet">

    @if ($tgl!='sebelumnya')
      @if (count($daftarNota['lunas'])<=0)
        <div class="text-center">
          <h3>
            Tidak Ada Nota Di tanggal {{ $tgl }}
          </h3>
        </div>
      @else
        <div class="table-responsive">

          <table class="table">
            <thead>
              <th class="col-md-2"></th>
              <th>Nomor Meja</th>
              <th>Kasir</th>
              <th>Waktu Pembayaran</th>
              <th>Diskon</th>
              <th>Total</th>
              <th>PPn 10%</th>
              <th>Total Akhir</th>
            </thead>
            <tbody>
              @foreach ($daftarNota['lunas'] as $key => $value)
                <tr>
                  <td>
                    @if ($value->status=='batal')
                      <a href="{{ url('/nota/'.$value->id.'/edit') }}" class="btn btn-danger ">Nota Batal</a>
                    @else
                      <a href="/print/nota/{{ $value->id }}" target="_blank" class="btn btn-success">Print</a>
                      <a href="{{ url('/nota/'.$value->id.'/edit') }}" class="btn btn-warning ">Detail</a>
                    @endif
                  </td>
                  <td>
                    {{ $value->nomor_meja }}
                  </td>
                  <td>
                    {{ $value->inputter->nama }}
                  </td>
                  <td>
                    {{ $value->updated_at->toTimeString() }}
                  </td>
                  <td>
                    {{ number_format($value->diskon) }}
                  </td>
                  <td>
                    {{ number_format($value->total) }}
                  </td>
                  <td>
                    {{ number_format($value->ppn) }}
                  </td>
                  <td>
                    {{ number_format($value->ppn + $value->total) }}
                  </td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td colspan="3" ></td>
                <td >Total</td>
                <td >{{ number_format($daftarNota['total']['diskon']) }}</td>
                <td >{{ number_format($daftarNota['total']['total']) }}</td>
                <td>{{ number_format($daftarNota['total']['ppn']) }}</td>
                <td>{{ number_format($daftarNota['total']['total'] + $daftarNota['total']['ppn']) }}</td>
              </tr>
            </tfoot>
          </table>
        </div>
      @endif
    @endif

  </div>
@endsection
