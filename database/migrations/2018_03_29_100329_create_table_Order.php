<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idNota')->unsigned();
            $table->integer('idBarang')->unsigned();
            $table->integer('kuantitas');
            $table->integer('harga_jual');
            $table->integer('diskon')->default(0);
            $table->text('catatan');
            $table->enum('status',['new','cancel','updated','send'])->default('new');
            $table->timestamps();

            $table
            -> foreign('idNota')
            -> references('id') -> on('nota');

            $table
            -> foreign('idBarang')
            -> references('id') -> on('barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order');
    }
}
