<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nomor_meja');
          $table->integer('input_by')->unsigned();
          $table->integer('diskon')->default(0);
          $table->integer('total')->default(0);
          $table->integer('ppn')->default(0);
          $table->integer('kembalian')->default(0);
          $table->enum('status',['proses','batal','lunas'])->default('proses');
          $table->timestamps();

          $table
          -> foreign('input_by')
          -> references('id') -> on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nota');
    }
}
