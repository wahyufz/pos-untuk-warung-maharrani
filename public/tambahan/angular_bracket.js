var App = angular.module('App', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('{+');
    $interpolateProvider.endSymbol('+}');
});
