<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    //mendeklarasikan nama table pada database
    protected $table = 'nota';

   //mendeklarasikan field apa saja yang bisa di
   protected $fillable = [
       'id',
       'nomor_meja',
       'input_by',
       'ket'
   ];
   public function inputter()
   {
     return $this->belongsTo('App\User','input_by');
   }
   public function order()
   {
     return $this->hasMany('App\Order','idNota');
   }
   public function kembalian()
   {
     return $this->hasOne('App\Kembalian','idNota');
   }

}
