<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kembalian extends Model
{
  //mendeklarasikan nama table pada database
  protected $table = 'kembalian';

 //mendeklarasikan field apa saja yang bisa di
 protected $fillable = [
     'id',
     'idNota',
     'kembalian',
     'bayar',
 ];
 public function nota()
 {
   return $this->belongsTo('App\Nota','idNota');
 }
}
