<?php

namespace App\Http\Middleware;

use Closure;

class cekUser
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next)
  {
    if (\Auth::guest()) {
      return redirect('/login');
    }elseif (\Auth::user()->posisi == 'dapur') {
      return redirect('/dapur');
    }
    return $next($request);
  }
}
