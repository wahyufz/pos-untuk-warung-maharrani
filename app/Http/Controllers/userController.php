<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//model user
use App\User;

use Validator;

use BackLink;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        BackLink::set();
        $daftarUser=User::orderBy('nama')->paginate(25);
        $jumlahUser=User::count();
        $status='seluruhnya';
        return view('pages.userIndexShow',compact('daftarUser','jumlahUser','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $route='tambah';
        return view('pages.userCreateEdit',compact('route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();

        $rules = array(
            'nama' => 'required|string|min:4',
            'password' => 'required|string|min:4',
            'password_confirm' => 'required|same:password',
            'posisi' => 'required',
            );

        $validator = Validator::make($input,$rules);


        if ($validator->fails()) {
            return redirect('user/create')
            ->withInput()
            ->withErrors($validator);
        }

        User::create([
            'nama' => $input['nama'],
            'password' => bcrypt($input['password']),
            'posisi' => $input['posisi'],
        ]);

        return BackLink::get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($status)
    {
        BackLink::set();
        $daftarUser=User::where('status',$status)->orderBy('nama')->paginate(25);
        $jumlahUser=User::where('status',$status)->get()->count();
        return view('pages.userIndexShow',compact('daftarUser','jumlahUser','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        $route='edit';
        return view('pages.userCreateEdit',compact('route','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);

        $input = $request->all();

        $rules = array(
            'nama' => 'required|string|min:4',
            'posisi' => 'required',
            );

        if ($input['password']) {
          $rules['password'] = 'required|string|min:4';
          $rules['password_confirm'] = 'required|same:password';
        }

        $validator = Validator::make($input,$rules);


        if ($validator->fails()) {
            return redirect('user/'.$id.'/edit')
            ->withInput()
            ->withErrors($validator);
        }

        $user->update($request->all());

        return BackLink::get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user=User::find($id);
        if ($user->status== 'aktif') {
            $user->status = 'nonaktif';
            $user->save();
        }elseif ($user->status == 'nonaktif') {
            $user->status = 'aktif';
            $user->save();
        }
        return BackLink::get();
    }
}
