<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Nota;

use App\Barang;

use App\Order;

use BackLink;

use Carbon\Carbon;

use App\Helper\Flashy;

use App\Helper\Total;

class orderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idNota)
    {
      $judul = "Tambah";
      BackLink::set();
      $nota = Nota::find($idNota);
      $barangTerpilih = Order::where('idNota',$idNota)->Where('status','!=','batal')->get();
      if ($barangTerpilih->count() > 0) {
        foreach ($barangTerpilih as $key => $value) {
          $terpilih []= $value->idBarang;
        }
        $daftarBarang = Barang::whereNotIn('id', $terpilih)->where('jenis','!=','mentah')->get();
      }
      else {
        $daftarBarang = Barang::where('jenis','!=','mentah')->get();
      }
      return view('pages.orderCreateEdit',compact('daftarBarang','judul','nota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token','submit');
        //filter kepada input text yang kosong
        foreach ($input as $key => $value) {
          if ($value['idBarang'] == 0 or $value['kuantitas'] == 0) {
            $idNota = $value['idNota'];
            unset($input[$key]);
          }
        }
        foreach ($input as $key => $value) {
          $tesGanda = Order::where('idNota',$value['idNota'])->where('idBarang',$value['idBarang'])->where('status','!=','batal')->get()->count();
          if ($tesGanda == 0) {
            $value['catatan'] = str_replace(array("\r\n", "\r", "\n"), "<br />", $value['catatan']);
            $value['harga_jual'] = Barang::find($value['idBarang'])->harga_jual;
            Order::create($value);
          }
        }

        return redirect('/nota/'.$idNota.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $nota = Nota::find($order->idNota);
        $barangTerpilih = Order::where('idNota',$order->idNota)->Where('id','!=',$id)->where('status','!=','batal')->get();
        if ($barangTerpilih->count() > 0) {
          foreach ($barangTerpilih as $key => $value) {
            $terpilih []= $value->idBarang;
          }
          $daftarBarang = Barang::whereNotIn('id', $terpilih)->get();
        }
        else {
          $daftarBarang = Barang::all();
        }
        $judul = "Edit";
        BackLink::set();
        return view('pages.orderCreateEdit',compact('daftarBarang','judul','order','nota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token','_method');
        $order = Order::find($id);
        foreach ($input as $key => $value) {
          $order['idBarang'] = $value['idBarang'];
          $order['kuantitas'] = $value['kuantitas'];
          $order['diskon'] = $value['diskon'];
          $order['catatan'] = str_replace(array("\r\n", "\r", "\n"), "<br />", $value['catatan']);
          $order->save();
        }
        return redirect('/nota/'.$order['idNota'].'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order['status']='batal';
        $order['kuantitas'] = 0;
        $order['harga_jual'] = 0;
        $order['diskon'] = 0;
        $order->save();
        return BackLink::get();
    }

    public function massDestroy(Request $request)
    {
        $array = $request->only('pesanan');
        foreach ($array['pesanan'] as $key => $value) {
          $hapus[] = $key;
        }
        $arrayHapus = Order::whereIn('id',$hapus)->get();
        foreach ($arrayHapus as $key => $value) {
          $order['status']='batal';
          $order['kuantitas'] = 0;
          $order['harga_jual'] = 0;
          $order['diskon'] = 0;
          $order->save();
        }
        return BackLink::get();
    }
}
