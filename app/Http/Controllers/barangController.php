<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//model Barang
use App\Barang;

use Session;

use Validator;

use BackLink;

class barangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarBarang=Barang::where('jenis','!=','Mentah')->orderBy('nama')->paginate(50);
        $judul='seluruhnya';
        BackLink::set();
        return view('pages.barangIndexShow',compact('daftarBarang','judul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route='tambah';
        return view('pages.barangCreateEdit',compact('route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = array(
            'nama' => 'required|string|min:4',
            'jenis' => 'required',
            'harga_jual' => 'required',
            );

        $validator = Validator::make($input,$rules);


        if ($validator->fails()) {
            return redirect('barang/create')
            ->withInput()
            ->withErrors($validator);
        }

        if ($input['jenis']=='Mentah') {
          $input['harga_jual']=0;
        }
        $baru = Barang::create($input);
        $baru->save();
        return BackLink::get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($param)
    {
        BackLink::set();
        // $tes=Session::get('last_visit');
        $param = explode("_",$param);
        $jenis = $param[0];
        $status = $param[1];
        $judul = $param[0]." ".$param[1];
        $daftarBarang=Barang::where('jenis','=',$jenis)->where('status','=',$status)->orderBy('nama','asc')->paginate(50);
        $jumlahBarang=Barang::where('jenis','=',$jenis)->where('status','=',$status)->get()->count();
        return view('pages.barangIndexShow',compact('daftarBarang','jumlahBarang','judul','jenis','param'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang=Barang::find($id);
        $route='edit';
        return view('pages.barangCreateEdit',compact('route','barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang=Barang::find($id);

        $input = $request->all();

        $rules = array(
            'nama' => 'required|string|min:4',
            'jenis' => 'required',
            'harga_jual' => 'required',
            );

        $validator = Validator::make($input,$rules);

        if ($validator->fails()) {
            return redirect('barang/'.$id.'/edit')
            ->withInput()
            ->withErrors($validator);
        }
        if ($input['jenis']=='Mentah') {
          $input['harga_jual']=0;
        }
        $barang->update($input);

        return BackLink::get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        if ($barang->status == 'aktif') {
            $barang->status = 'nonaktif';
            $barang->save();
        }elseif ($barang->status == 'nonaktif') {
            $barang->status = 'aktif';
            $barang->save();
        }
        return BackLink::get();
    }

    public function search(Request $request)
    {
      $keyWords['k'] = $request['k'];
      $keyWords['jns'] = $request['jns'];
      $barang=Barang::where('nama','like','%'.$keyWords['k'].'%')->where('jenis','like','%'.$keyWords['jns'].'%')->orderBy('status')->orderBy('nama')->paginate(25);
      return view('pages.barangSearch',compact('barang','keyWords'));
    }

}
