<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Nota;
use App\Helper\CekPrinter;
use Carbon\Carbon;

class printController extends Controller
{
    public function pesanan(Request $request)
    {
      return $request->all();
    }

    public function nota($idNota)
    {
      return CekPrinter::pembayaran($idNota);
    }
}
