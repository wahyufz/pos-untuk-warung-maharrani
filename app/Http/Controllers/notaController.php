<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Nota;

use App\Barang;

use App\Order;

use Session;

use Auth;

use Validator;

use BackLink;

use Carbon\Carbon;

use App\Helper\Flashy;

use App\Helper\Total;

use App\Helper\PrintNota;

use Illuminate\Support\Facades\Input;

class notaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $judul='Hari ini';
      BackLink::set();
      $daftarNota['proses'] = Nota::where('created_at', '>=', Carbon::today())->where('status','proses')->orderBy('created_at')->get();
      $daftarNota['lunas'] = Nota::where('created_at', '>=', Carbon::today())->where('status','!=','proses')->orderBy('created_at','DESC')->get();
      $daftarNota['total'] = Total::getPerHari(Carbon::today()->toDateString());
      return view('pages.notaIndexShow',compact('daftarNota','judul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      //format nomor_meja 'tanggal_nota/nomor_nota/nomor_meja'
      $tgl = Carbon::today()->format('ymd');
      $notaHariIni = Nota::where('nomor_meja','like',$tgl.'%')->get()->count();
      $nomorNota = $tgl.'/'.str_pad($notaHariIni+1, 3, '0', STR_PAD_LEFT).'/'.str_pad($input['nomor_meja'], 3, '0', STR_PAD_LEFT);
      Nota::create([
        'nomor_meja' => $nomorNota,
        'input_by' => Auth::user()->id
      ]);
      return BackLink::get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if ($id != 'lunas' || $id != 'tambah' ) {
        return redirect('/nota');
      }
      $daftarNota = Nota::where('ket', '=', $id)->where('created_at', '>=', Carbon::today())->orderBy('created_at')->paginate(10);
      $judul=$id;
      BackLink::set();
      return view('pages.notaIndexShow',compact('daftarNota','judul'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //update harga pesanan di nota (proses) sesuai dengan harga jual  barang
      $nota = Nota::find($id);
      if ($nota->status == 'proses') {
        if ($nota->order->count() > 0) {
          foreach ($nota->order as $key => $value) {
            $order = Order::find($value->id);
            if ($order->status!='batal') {
              $order['harga_jual'] = $order->barang->harga_jual;
              $order->save();
            }
          }
        }
        //ambil kembali data yang sudah diupdate
        $nota = Nota::find($id);
        $nota->akhir = Total::getPerNota($id);
      }
      if ($nota->status == 'proses') {
        $judul = "Edit";
      }else {
        $judul = "Rincian";
      }
      BackLink::set();
      return view('pages.notaEdit',compact('nota','judul'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $nota = Nota::find($id);
      $input = $request->only('bayar','kembalian','ppn');
      $nota['diskon']=$nota->order->pluck('diskon')->sum();
      $nota['total'] = $input['bayar'] - $input['ppn'] - $input['kembalian'];
      $nota['ppn'] = $input['ppn'];
      $nota['kembalian'] = $input['kembalian'];
      $nota['status'] = 'lunas';
      $nota['input_by'] = Auth::user()->id;
      $nota->save();
      return redirect('/nota');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $nota = Nota::find($id);
      Flashy::set('merah','nota telah berhasil dihapus');
      $nota->status='batal';
      $nota->save();
      return redirect('/nota');
    }

    public function lunas()
    {
      BackLink::set();
      $tgl = Input::get('tgl');
      if ($tgl == null) {
        $tgl = 'sebelumnya';
        return view('pages.notaSebelum',compact('tgl'));
      }else {
        $daftarNota['lunas'] = Nota::where('created_at','like',$tgl.'%')->where('status','!=','proses')->orderBy('created_at')->get();
        $daftarNota['total'] = Total::getPerHari($tgl);
        return view('pages.notaSebelum',compact('daftarNota','tgl'));
      }
    }
}
