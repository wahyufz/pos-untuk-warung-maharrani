<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();
Route::get('/', 'HomeController@index');
Route::get('/dapur', 'printController@dapur');

Route::group(['middleware' => 'cekUser'], function () {
	Route::get('/setting', 'settingController@setting');

	Route::post('/print/pesanan', 'printController@pesanan');
	Route::get('/print/nota/{idNota}', 'printController@nota');
	Route::resource('testing', 'testingController');

	Route::resource('user', 'userController');

	Route::get('/search/barang', 'barangController@search');
	Route::resource('barang', 'barangController');

	Route::get('/order/{idNota}/create', 'orderController@create');
	Route::get('/order/{idNota}/edit', 'orderController@edit');
	Route::post('/order', 'orderController@store');
	Route::patch('/order/{id}', 'orderController@update');
	Route::delete('/order/hapusbanyak', 'orderController@massDestroy');
	Route::delete('/order/{id}', 'orderController@destroy');

	Route::get('/nota/lunas', 'notaController@lunas');
	Route::resource('nota', 'notaController');
});
