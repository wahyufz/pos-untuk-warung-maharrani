<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //mendeklarasikan nama table pada database
    protected $table = 'barang';

   //mendeklarasikan field apa saja yang bisa di
   protected $fillable = [
       'nama',
       'jenis',
       'harga_jual',
       'status'
   ];

   //Accessor untuk nama
   public function getNamaAttribute($nama){
       return ucwords($nama);
   }

   //Mutator untuk nama
   public function setNamaAttribute($nama){
       $this->attributes['nama'] = strtolower($nama);
   }

   public function order()
   {
     return $this->hasMany('App\Order','idBarang');
   }

}
