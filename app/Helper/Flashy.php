<?php

namespace App\Helper;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;

class Flashy {

    public static function set($type,$message) {
      switch ($type) {
        case 'hijau':
          $alert = 'alert-success';
          break;

        case 'biru':
          $alert = 'alert-info';
          break;

        case 'kuning':
          $alert = 'alert-warning';
          break;

        case 'merah':
          $alert = 'alert-danger';
          break;

        default:
          $alert = 'alert-danger';
          $message = 'type yang anda masukkan salah';
          break;

      }

      Session::flash('message', $message);
      Session::flash('alert-class', $alert);
    }
}
