<?php

namespace App\Helper;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Nota;
use App\Helper\CekPrinter;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

class CekPrinter {

  public function __construct()
  {
  }

  //cek apakah directory printer ada atau tidak
  public static function cekDir()
  {
    $path = public_path('dev/printer_nota');
    $printer = file_exists($path);
    if ($printer == null) {
      return false;
    }
    return true;
  }

  //cek printer yang tersedia di komputer
  public static function printerConnect()
  {
    if (CekPrinter::cekDir() == false) {
      return null;
    }else {
      $path = public_path('dev/printer_nota');
      $printer = array_diff(scandir($path,1), array('.', '..'));
      $printerPath = $path.'/'.implode($printer);
      $connector = new FilePrintConnector($printerPath);
      $printer = new Printer($connector);
      return $printer;
    }
  }

  //cetak di printer kasir
  public static function pembayaran($idNota)
  {
    function harga($left,$right,$mode)
    {
        $sumleft=strlen($left);
        if ($sumleft>=20) {
            $sumleft=0;
        }

        $sumright=strlen($right);
        if ($mode=="h") {
        	$mode=33;
        }else if ($mode=="b") {
        	$mode=40;
        }
        $distance=$mode-$sumleft-$sumright;
        $space="";
        for ($i=0; $i < $distance ; $i++) {
        	$space=$space." ";
        }
        if ($sumleft==0) {
            return $left."\n".$space.$right;
        }
        else{
            return $left.$space.$right;
        }
    }
    //font b = 40 karakter
    //double height = 33
    $nota = Nota::find($idNota);
    $printer = CekPrinter::printerConnect();
    if ($printer == null) {
      return "Printer Belum Tersambung <br />
      <a href='javascript:window.location.reload();'>Muat Ulang</a>
      ";
    }else {
      $week = array(
          '0' => 'Minggu',
          '1' => 'Senin',
          '2' => 'Selasa',
          '3' => 'Rabu',
          '4' => 'Kamis',
          '5' => 'Jum\'at',
          '6' => 'Sabtu',
           );
      $printer -> initialize();
      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
      $printer -> text("\nWARUNG MAHARRANI"."\n");
      $printer -> selectPrintMode(Printer::MODE_FONT_B);
      $printer -> text("Jl. Songgon (Perbatasan Cemoro)"."\n");
      $printer -> text("0857 5567 4894"."\n");
      $printer -> text("========================================"."\n");
      $printer -> setJustification(Printer::JUSTIFY_LEFT); // Reset
      $printer -> text($week[date("w")].", ".$nota->created_at->format('d-m-y')."\n");
      $printer -> text("Nota Nomor ".$nota->nomor_meja."\n");
      $printer -> text("Kasir ".$nota->inputter->nama."\n");
      $printer -> text("----------------------------------------"."\n");
      foreach ($nota->order as $key => $value) {
        $printer->text($value->barang['nama']."\n");
        $printer -> setJustification(Printer::JUSTIFY_RIGHT);
        $printer->text(number_format($value->harga_jual)." x ".$value->kuantitas." = ".(number_format($value->harga_jual*$value->kuantitas))."\n");
        if ($value->diskon > 0) {
          $printer->text(number_format("(".$value->diskon).")\n");
        }
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
      }
      $printer -> text("----------------------------------------"."\n");
      $printer -> text(harga("TOTAL PESANAN",number_format($nota->total),"b")."\n");
      $printer -> text(harga("PPN 10%",number_format($nota->ppn),"b")."\n");
      $printer -> text(harga("TOTAL BAYAR",number_format($nota->total+$nota->ppn),"b")."\n");
      $printer -> text(harga("BAYAR",number_format($nota->total+$nota->ppn+$nota->kembalian),"b")."\n");
      $printer -> text(harga("KEMBALI",number_format($nota->kembalian),"b")."\n");
      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer -> text("========================================"."\n");
      $printer -> text("Terima kasih telah berbelanja di Warung \nMaharrani"."\n");
      $printer -> cut();
      $printer -> close();
      return "<body onload=window.setTimeOut('window.close();',5000)>
      nota ".$nota->nomor_meja." berhasil di Cetak<br>
      </body>";
    }
  }

  //cetak di printer dapur dari prinJob
  public static function pelanggan()
  {
    $printer = CekPrinter::printerConnect();
    if ($printer == null) {
      return "Printer Belum Tersambung";
    }else {
      $a = $printer -> getPrinterStatus(Escpos::STATUS_PAPER_ROLL);
      // $printer -> setJustification(Printer::JUSTIFY_CENTER);
      // $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
      // $printer -> text("Pelanggan Baru \n nomor meja ".$nomor_meja."\n");
      // $printer -> cut();
      // $printer -> close();
      return $a;
    }
  }

  //cetak di printer dapur dari prinJob
  public static function pesananBaru($nota)
  {

  }

  //cetak di printer dapur dari prinJob
  public static function gantiMenu($nota)
  {

  }

  //cetak di printer dapur dari prinJob
  public static function hapusMenu($nota)
  {

  }
}
