<?php

namespace App\Helper;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Order;
use App\Nota;

class Total {

    public static function getPerNota($idNota) {
      $arrayOrder = Order::where('idNota',$idNota)->get();
      $totalNota['diskon'] = $arrayOrder->pluck('diskon')->sum();
      $totalNota['total'] = 0;
      foreach ($arrayOrder as $key => $value) {
        $totalNota['total']   += (($value->harga_jual * $value->kuantitas) - $value->diskon);
      }
      return $totalNota;
    }
    public static function getPerHari($tgl)
    {
      $arrayNota = Nota::Where('created_at','like',$tgl.'%')->where('status','lunas')->get();
      $totalHarian['total'] = $arrayNota->pluck('total')->sum();
      $totalHarian['diskon'] = $arrayNota->pluck('diskon')->sum();
      $totalHarian['ppn'] = $arrayNota->pluck('ppn')->sum();
      return $totalHarian;
    }
}
