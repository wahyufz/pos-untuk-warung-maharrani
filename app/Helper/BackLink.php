<?php

namespace App\Helper;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;

class BackLink {
    /*
    * function set() digunakan untuk memasukkan halaman
    * terakhir ke session last_visit
    *
    * link untuk kembali ke halaman sebelumnya
    * pasang di Index, Show dan sejenisnya
    */
    public static function set() {
      $url =\Request::path();
      Session::set('last_visit',$url);
    }

    /*
    * function get() digunakan untuk redirect ke halaman sebelumnya
    * yang telah tersimpan di session last_visit
    *
    * pasang di Store, Update, Destroy dan sejenisnya
    */
    public static function get() {
      $redirect_link=session()->get('last_visit');
      return redirect($redirect_link);
    }
}
