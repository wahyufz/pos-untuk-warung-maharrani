<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  //mendeklarasikan nama table pada database
  protected $table = 'order';

 //mendeklarasikan field apa saja yang bisa di
  protected $fillable = [
     'id',
     'idNota',
     'idBarang',
     'kuantitas',
     'catatan',
     'diskon'
 ];
 public function nota()
 {
   return $this->belongsTo('App\Nota','idNota');
 }
 public function barang()
 {
   return $this->belongsTo('App\Barang','idBarang');
 }
}
