<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     //mendeklarasikan nama table pada database
     protected $table = 'user';

    //mendeklarasikan field apa saja yang bisa di
    protected $fillable = [
        'nama',
        'password',
        'posisi',
        'ket'
    ];

    protected $hidden = [
      'password','remember_token'
    ];

    public function inputNota()
    {
      return $this->hasMany('App\Nota','input_by');
    }

    //Accessor
    public function getNamaAttribute($nama){
        return ucwords($nama);
    }

    //Mutator
    public function setNamaAttribute($nama){
        $this->attributes['nama'] = strtolower($nama);
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
